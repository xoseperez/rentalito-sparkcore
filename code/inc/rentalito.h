#define VALUE_LENGTH 10

// MQTT mqtt_channel variables
struct mqtt_channel {
    char * topic;
    char * title;
    char * units;
    uint8_t decimals;
    unsigned long expiration_time;
    char value[VALUE_LENGTH];
    unsigned long last_time;
};

struct mqtt_event {
   char * topic;
   char * value;
   char * message;
};

//-----------------------------------------------------------------------------
// CONFIGURATION
//-----------------------------------------------------------------------------

// Configure connection to MQTT broker
byte mqtt_broker[] = { 192, 168, 1, 100 };
int mqtt_port = 1883;
char mqtt_client_id[] = "rentalito";

// Define topics
//char power_topic[] = "/benavent/general/power";
char remote_temperature_topic[] = "/benavent/outdoor/temperature/bmp085";
char remote_humidity_topic[] = "/benavent/outdoor/humidity/dht22";
char remote_pressure_topic[] = "/benavent/outdoor/pressure/bmp085";
//char remote_dewpoint_topic[] = "/benavent/outdoor/dewpoint/dht22";
//char local_temperature_topic[] = "/benavent/studio/temperature";
//char local_humidity_topic[] = "/benavent/studio/humidity";
char solar_panel_voltage_topic[] = "/benavent/outdoor/voltage/solarpanel";
char cpm_topic[] = "/benavent/general/radioactivity/cpm";
char door_topic[] = "/benavent/door/status";
char time_topic[] = "/benavent/time";
char status_topic[] = "/service/rentalito";
char subscribe_to[] = "/benavent/#";

// Configure mqtt_channels (topic, title, units, decimals, expiration_time)
//mqtt_channel power_mqtt_channel = {power_topic, "CONSUM", "W", 0, 150, "", 0};
mqtt_channel remote_temperature_mqtt_channel = {remote_temperature_topic, "TEMPERATURA", "C", 1, 750, "", 0};
mqtt_channel remote_humidity_mqtt_channel = {remote_humidity_topic, "HUMITAT", "%", 0, 750, "", 0};
mqtt_channel remote_pressure_mqtt_channel = {remote_pressure_topic, "PRESSIO ATM.", "hPa", 0, 750, "", 0};
//mqtt_channel remote_dewpoint_mqtt_channel = {remote_dewpoint_topic, "PUNT ROSADA", "C", 1, 750, "", 0};
//mqtt_channel local_temperature_mqtt_channel = {local_temperature_topic, "TEMP. INT.", "C", 1, 750, "", 0};
//mqtt_channel local_humidity_mqtt_channel = {local_humidity_topic, "HUMITAT INT.", "%", 0, 750, "", 0};
mqtt_channel solar_panel_voltage_mqtt_channel = {solar_panel_voltage_topic, "PANELL SOLAR", "mV", 0, 750, "", 0};
mqtt_channel cpm_mqtt_channel = {cpm_topic, "RADIACIO", "CPM", 0, 150, "", 0};
mqtt_channel time_mqtt_channel = {time_topic, "HORA", "", 0, 60, "", 0};

// Add mqtt_channels
mqtt_channel mqtt_channels[] = {
	//power_mqtt_channel,
	remote_temperature_mqtt_channel,
	remote_humidity_mqtt_channel,
	remote_pressure_mqtt_channel,
	//remote_dewpoint_mqtt_channel,
	//local_temperature_mqtt_channel,
	//local_humidity_mqtt_channel,
	solar_panel_voltage_mqtt_channel,
	cpm_mqtt_channel,
   time_mqtt_channel
};

// Configure mqtt_events
mqtt_event door_mqtt_event = {door_topic, "1", "PORTA"};

// Add mqtt_events
mqtt_event mqtt_events[] = {
   door_mqtt_event
};
