#define MATRIX_ENABLED
//#define DHT22_ENABLED
#define MQTT_ENABLED
//#define IR_ENABLED
//#define RENTALITO_DEBUG_USB
//#define RENTALITO_DEBUG_DASHBOARD

#include "application.h"
#include "rentalito.h"
#ifdef MQTT_ENABLED
    #include "MQTT.h"
#endif
#ifdef DHT22_ENABLED
    #include "DHT22.h"
#endif
#ifdef MATRIX_ENABLED
    #include "ht1632c.h"
#endif

// Display management variables

#define DISPLAY_UPDATE_INTERVAL 10000
#define DISPLAY_ABOUT_EVERY     10

#define MATRIX_DATA_PIN         D3
#define MATRIX_CLK_PIN          D6
#define MATRIX_CS_PIN           D5
#define MATRIX_WR_PIN           D4

#define DEFAULT_BRIGHTNESS      15

boolean display_on = true;
byte display_brightness = DEFAULT_BRIGHTNESS;
unsigned long display_timer = millis() + DISPLAY_UPDATE_INTERVAL;
byte num_mqtt_channels = 0;
byte num_mqtt_events = 0;
byte pointer = 0;

#ifdef MATRIX_ENABLED

ht1632c matrix = ht1632c(MATRIX_DATA_PIN, MATRIX_WR_PIN, MATRIX_CLK_PIN, MATRIX_CS_PIN, 2);

#endif

#ifdef MQTT_ENABLED

void mqtt_callback(char* topic, byte* payload, unsigned int length);
MQTT client(mqtt_broker, mqtt_port, mqtt_callback);
char * buffer;

#endif

// IR remote variables

#define IR_PIN D0
#define IR_DEBOUNCE 50

#define BIT_0 1500
#define BIT_1 2500
#define REMOTE_CHECK 0x47

#define STATUS_IDLE 0
#define STATUS_DECODING 1
#define STATUS_READY 2

byte volatile ir_status = STATUS_IDLE;

#ifdef IR_ENABLED

unsigned long volatile ir_previous = 0;
unsigned long volatile ir_data[16];
byte volatile ir_pulses = 0;

unsigned long ir_timer = millis() + IR_DEBOUNCE;

#endif

// DHT22 variables

#define DHTPIN D1
#define DHTTYPE DHT22
#define DHT_UPDATE_INTERVAL 300000

#ifdef DHT22_ENABLED

DHT dht(DHTPIN, DHTTYPE);
unsigned long dht_timer = millis() + 10000; // first temp check in 10 seconds

#endif

// Display methods

void show() {

    mqtt_channel s = mqtt_channels[pointer];

    #ifdef MATRIX_ENABLED

        matrix.clear();

        matrix.setFont(FONT_5x7);
        matrix.putText(0, 1, s.title, RED, ALIGN_CENTER);

        matrix.setFont(FONT_8x8);
        matrix.putText(0, matrix.getDisplayHeight() - matrix.getFontHeight() + 1, s.value, GREEN, ALIGN_CENTER);

        matrix.sendframe();

    #endif

    #ifdef RENTALITO_DEBUG_USB
        Serial.print(s.title);
        Serial.print(" - ");
        Serial.println(s.value);
    #endif

}

void show_message(char * message) {
   #ifdef MATRIX_ENABLED
      matrix.clear();
      matrix.setFont(FONT_8x8);
      if (strlen(message) > 8) {
         matrix.hScroll(4, message, ORANGE, SCROLL_LEFT, 16);
      } else {
         matrix.putText(0, 4, message, ORANGE, ALIGN_CENTER);
         display_timer = millis() + DISPLAY_UPDATE_INTERVAL;
      }
   #endif
}

void show_about() {
   show_message("tinkerman.eldiariblau.net");
}

void clear(bool show_message) {

    #ifdef MATRIX_ENABLED
        matrix.clear();
        if (show_message) {
            matrix.setFont(FONT_5x7);
            matrix.putText(0, 1, "WAITING", RED, ALIGN_CENTER);
        }
        matrix.sendframe();
    #endif

    #ifdef RENTALITO_DEBUG_USB
        Serial.println("Display cleared");
    #endif

}

void brightness(int increment) {

    display_brightness = constrain(display_brightness + increment, 0, 15);

    #ifdef MATRIX_ENABLED
        matrix.setBrightness(display_brightness);
    #endif

    #ifdef RENTALITO_DEBUG_USB
        Serial.print("Brightness set to ");
        Serial.println(display_brightness);
    #endif

}

void find(byte increment) {

    byte start_pointer = pointer;
    static byte reset_count = 0;
    boolean found = false;

    do {
        pointer = (pointer + increment) % num_mqtt_channels;
        if (mqtt_channels[pointer].last_time > 0 &&
            millis()/1000 - mqtt_channels[pointer].last_time < mqtt_channels[pointer].expiration_time) {
            found = true;
            break;
        }
    } while (pointer != start_pointer);

    if (found) {
        reset_count = 0;
        show();
    } else {
        reset_count++;
        if (reset_count == 6) NVIC_SystemReset();
        clear(true);
    }

}

void next() {
    find(1);
}

void previous() {
    find(-1);
}

void toggle() {
    display_on = !display_on;
    if (display_on) {
        next();
        display_timer = millis() + DISPLAY_UPDATE_INTERVAL;
    } else {
        clear(false);
    }
}

// MQTT methods

#ifdef MQTT_ENABLED

void mqtt_callback(char* topic, byte* payload, unsigned int length) {

    length = length >= VALUE_LENGTH ? VALUE_LENGTH-1 : length;
    char message[VALUE_LENGTH] = {0};
    memcpy(message, payload, length);

    #ifdef RENTALITO_DEBUG_DASHBOARD
		Spark.publish(topic, message);
      delay(100);
    #endif

    byte position = -1;

    // Find mqtt_event
    for (byte i=0; i<num_mqtt_events; i++) {
        if (strcmp(topic, mqtt_events[i].topic) == 0) {
           if (strcmp(message, mqtt_events[i].value) == 0) {
             show_message(mqtt_events[i].message);
           }
           return;
        }
    }

    // Find mqtt_channel
    for (byte i=0; i<num_mqtt_channels; i++) {
        if (strcmp(topic, mqtt_channels[i].topic) == 0) {
            position = i;
            break;
        }
    }

    if (position > -1) {

        // Copy value
        memcpy(mqtt_channels[position].value, message, VALUE_LENGTH);

        // Start copying the units over the decimal point
        uint8_t dot;
        for (dot=0; dot < length; dot++) {
            if (payload[dot] == '.') break;
        }
        if (mqtt_channels[position].decimals > 0) {
            dot = min(dot + mqtt_channels[position].decimals + 1, length);
        }
        memcpy(mqtt_channels[position].value + dot, mqtt_channels[position].units, strlen(mqtt_channels[position].units) + 1);

        // Update timestamp
        mqtt_channels[position].last_time = millis() / 1000;

    }

}

#endif

// IR methods

#ifdef IR_ENABLED

void ir_int() {

    if (ir_status == STATUS_READY) return;

    unsigned long now = micros();
    unsigned long width = now - ir_previous;

    if (width > BIT_1) {
        ir_pulses = 0;
        ir_status = STATUS_IDLE;
    } else {
        ir_data[ir_pulses++] = width;
        ir_status = (ir_pulses == 16) ? STATUS_READY : STATUS_DECODING;
    }

    ir_previous = now;

}

int ir_decode() {
    unsigned int result = 0;
    for (byte i = 0 ; i < 16 ; i++)
        if (ir_data[i] > BIT_0) result |= (1<<i);
    if (REMOTE_CHECK != (result & REMOTE_CHECK)) return 0;
    return result >> 8;
}

#endif

#ifdef DHT22_ENABLED

void readDHT() {

    char buffer[10];
    float t, h;

    t = dht.readTemperature();
    h = dht.readHumidity();

    if (t != NAN && t != NAN) {

        sprintf(buffer, "%.1f", t);
        #ifdef MQTT_ENABLED
            client.publish(local_temperature_topic, buffer);
        #endif
        #ifdef RENTALITO_DEBUG_USB
            Serial.print("LOCAL TEMPERATURE: ");
            Serial.println(buffer);
        #endif

        sprintf(buffer, "%d", (int) h);
        #ifdef MQTT_ENABLED
            client.publish(local_humidity_topic, buffer);
        #endif
        #ifdef RENTALITO_DEBUG_USB
            Serial.print("LOCAL HUMIDITY: ");
            Serial.println(buffer);
        #endif

    #ifdef RENTALITO_DEBUG_USB
    } else {
        Serial.println("Error reading DHT22 sensor");
    #endif
    }

}

#endif

void setup() {

    #ifdef RENTALITO_DEBUG_USB
        Serial.begin(9600);
    #endif

    num_mqtt_channels = sizeof(mqtt_channels) / sizeof(mqtt_channel);
    num_mqtt_events = sizeof(mqtt_events) / sizeof(mqtt_event);

    #ifdef MATRIX_ENABLED
        matrix.begin();
        matrix.setBrightness(display_brightness);
    #endif

    #ifdef IR_ENABLED
        pinMode(IR_PIN, INPUT);
        attachInterrupt(IR_PIN, ir_int, FALLING);
    #endif

    #ifdef DHT22_ENABLED
        dht.begin();
    #endif

    #ifdef MQTT_ENABLED
        client.connect(mqtt_client_id, status_topic, MQTT::QOS0, 0, "0");
        if (client.isConnected()) {
            client.publish(status_topic, "1");
            client.subscribe(subscribe_to);
        }
    #endif

}

void loop() {

   static byte about_countdown = 0;

   if (ir_status != STATUS_DECODING) {

        #ifdef MQTT_ENABLED
            if (client.isConnected()) client.loop();
        #endif

        #ifdef MATRIX_ENABLED
            if (display_on) {
               matrix.scroll();
               if (!matrix.scrolling() && millis() > display_timer) {
                  about_countdown++;
                  if (about_countdown == DISPLAY_ABOUT_EVERY) {
                     about_countdown = 0;
                     show_about();
                  } else {
                     next();
                     display_timer = millis() + DISPLAY_UPDATE_INTERVAL;
                  }
               }
            }
        #endif

        #ifdef DHT22_ENABLED
            if (millis() > dht_timer) {
                readDHT();
                dht_timer = millis() + DHT_UPDATE_INTERVAL;
            }
        #endif

    }

    #ifdef IR_ENABLED
        if (ir_status == STATUS_READY) {

            if (millis() > ir_timer) {

                int key = ir_decode();

                switch(key)  {
                    case 10: next(); break;
                    case 18: previous(); break;
                    case 34: brightness(1); break;
                    case 42: brightness(-1); break;
                    case 2: toggle(); break;
                    default: break;
                }

            }

            ir_status = STATUS_IDLE;
            ir_timer = millis() + IR_DEBOUNCE;

        }
    #endif

}
